/* jshint strict: true */

'use strict';

const
  _ = require('lodash'),
  request = require('request'),
  jar = request.jar(),
  userAgents = require('./user-agents');
  
class Request {
  constructor(baseUrl, origin, referer) {
    this.setBaseUrl(baseUrl);
    this.setOrigin(origin);
    this.setReferer(referer || origin);
  }
  
  setBaseUrl(url) {
    this.baseUrl = url;
  }
  
  getBaseUrl() {
    return this.baseUrl;
  }
  
  setOrigin(origin) {
    return this.origin = origin;
  }
  
  getOrigin() {
    return this.origin;
  }
  
  setReferer(referer) {
    return this.referer = referer;
  }
  
  getReferer() {
    return this.referer || this.getOrigin();
  }
  
  headers(referer) {
    let headers = {'User-Agent': userAgents.pick()};
    referer = (referer) ? this.setReferer(referer) : this.getReferer();
    headers.Referer = referer;
    headers.Origin = this.getOrigin();
    headers.Accept = 'application/json, text/javascript, */*; q=0.01';
    return headers;
  }
  
  prepareUrl(url, includeBaseUrl = false, proto = 'http') {
    if(url.indexOf('http') < 0) {
      url = proto + "://" + url;
    }
    
    if(includeBaseUrl && url.indexOf(this.getBaseUrl()) < 0) {
      url = this.getBaseUrl() + url;
    }
    return url;
  }
  
  get(url, referer, callback) {
    if(!callback && referer) {
      callback = referer;
      referer = null
    }
    
    let preparedUrl = this.prepareUrl(url);
    
    console.log('GET:', url);
    
    request.get({url: preparedUrl, jar, headers: this.headers(referer)}, function(err, response, body) {
      if (err) {
        console.error(err);
        return callback(err);
      }
      
      if(!_.isEmpty(JSON.parse(body))) {
        body = JSON.parse(body);
      }
      callback(null, response, body);
    });
  }
  
  post(url, referer, callback) {
    if(!callback && referer) {
      callback = referer;
      referer = null
    }
    
    let preparedUrl = this.prepareUrl(url);
  
    console.log('POST:', url);
    
    request.post({url: preparedUrl, jar, headers: this.headers(referer)}, function(err, response, body) {
      if (err) {
        console.error(err);
        return callback(err);
      }
  
      if(!_.isEmpty(JSON.parse(body))) {
        body = JSON.parse(body);
      }
      callback(null, response, body);
    });
  }
  
  requestObject(url, referer) {
    return request({url, headers: this.headers(referer), jar});
  }
  
  pipeUrlToWriteStream(url, writeStream) {
    let preparedUrl = this.prepareUrl(url);
    return this.requestObject(preparedUrl).pipe(writeStream);
  }
}

module.exports = Request;