/* jshint strict: true */

'use strict';

const
  crypto = require('crypto'),
  crypter = require('./crypter');

class Hasher {

  static md5(data) {
    return crypto.createHash('md5').update(data.toString()).digest('hex');
  }

  static secretMd5(data) {
    return this.md5(this.md5(data) + crypter.getSecretKey());
  }

  static saltedMd5(data, salt) {
    return this.md5(this.md5(data) + salt);
  }
  
  static hash(data) {
    return this.secretMd5(crypter.encrypt(data));
  }

  static check(plain, hashed) {
    return this.hash(plain) === hashed;
  }
}

module.exports = Hasher;