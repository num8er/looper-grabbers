/* jshint strict: true */

'use strict';

const
  path = require('path'),
  fs = require('fs-extra'),
  async = require('async'),
  Request = require('./request');

class FS {
  constructor(rootPath) {
    this.setRootPath(rootPath);
  }
  
  setRootPath(rootPath = null) {
    if(!rootPath) {
      rootPath = path.join(__dirname, '..', '..');
    }
    this.rootPath = rootPath;
  }
  
  getRootPath() {
    return this.rootPath;
  }
  
  getStoragePath() {
    return path.join(this.getRootPath(), 'storage'); //  /root/path/storage
  }
  
  getLoopsPath() {
    return path.join(this.getStoragePath(), 'loops');
  }
  
  getLoopPath(loopId) {
    return path.join(this.getLoopsPath(), loopId); // /root/path/storage/loops/:loopId
  }
  
  getLoopImagesPath(loopId) {
    return path.join(this.getLoopPath(loopId), 'images'); // /root/path/storage/loops/:loopId/images
  }
  
  getLoopImagePath(loopId, imageId, ext = 'jpg') {
    return path.join(this.getLoopImagesPath(loopId), imageId)+'.'+ext; // /root/path/storage/loops/:loopId/images/:imageId
  }
  
  getLoopVideosPath(loopId) {
    return path.join(this.getLoopPath(loopId), 'video'); // /root/path/storage/loops/:loopId/images
  }
  
  getLoopVideoPath(loopId, videoId, ext = 'mp4') {
    return path.join(this.getLoopVideosPath(loopId), videoId)+'.'+ext; // /root/path/storage/loops/:loopId/images/:imageId
  }
  
  getLoopAudiosPath(loopId) {
    return path.join(this.getLoopPath(loopId), 'audio'); // /root/path/storage/loops/:loopId/images
  }
  
  getLoopAudioPath(loopId, audioId, ext = 'mp3') {
    return path.join(this.getLoopAudiosPath(loopId), audioId)+'.'+ext; // /root/path/storage/loops/:loopId/images/:imageId
  }
  
  getImagesPath() {
    return path.join(this.getStoragePath(), 'images');
  }
  
  getImagePath(imageId, ext = 'jpg') {
    return path.join(this.getImagesPath(), imageId)+'.'+ext;
  }
  
  createLoopPath(loopId, callback) {
    async.eachSeries([
        this.getLoopImagesPath(loopId),
        this.getLoopAudiosPath(loopId),
        this.getLoopVideosPath(loopId)
      ], fs.ensureDir, callback)
  }
  
  listLoopVideoFiles(loopId, callback) {
    let path = this.getLoopVideosPath(loopId);
    fs.stat(path, (err, stat) => {
      if(err) return callback(err);
      
      try {
        let files = fs.walkSync(path);
        return callback(null, files);
      }
      catch(ex) {
        callback(ex);
      }
    });
  }
  
  listLoopAudioFiles(loopId, callback) {
    let path = this.getLoopAudiosPath(loopId);
    fs.stat(path, (err, stat) => {
      if(err) return callback(err);
    
      try {
        let files = fs.walkSync(path);
        return callback(null, files);
      }
      catch(ex) {
        callback(ex);
      }
    });
  }
  
  listLoopImageFiles(loopId, callback) {
    let path = this.getLoopImagesPath(loopId);
    fs.stat(path, (err, stat) => {
      if(err) return callback(err);
    
      try {
        let files = fs.walkSync(path);
        return callback(null, files);
      }
      catch(ex) {
        callback(ex);
      }
    });
  }
  
  pipeRequestToFile(request, file, callback, replace = true) {
    async.waterfall([
      next => {
        if(replace) {
          return fs.remove(file, next);
        }
        next();
      },
      next => {
        var fileStream = fs.createWriteStream(file);
        fileStream
          .on('data', () => {})
          .on('error', next)
          .on('finish', next);
        request.pipe(fileStream);
      }
    ], callback);
  }
}

module.exports = FS;