/* jshint strict: true */

'use strict';

const
  _ = require('lodash'),
  async = require('async'),
  MongoClient = require('mongodb').MongoClient;

class Database {
  constructor(uri, callback) {
    const self = this;
    self.collections = {};
    MongoClient.connect(uri, (err, db) => {
      if(err) {
        console.error('MongoClient.connect err:', err);
        process.exit(-1);
        if(callback) callback(err, false);
        return;
      }
  
      console.log('MongoClient.connect success');
      if(callback) callback(null, true);
      self.db = db;
    });
  }
  
  connected() {
    return this.db ? true : false;
  }
  
  getCollection(collection, callback) {
    // if it's object so return as collection
    if(!_.isString(collection)) {
      return callback(null, collection);
    }
    
    // cached collections object
    if(this.collections[collection]) {
      return callback(null, this.collections[collection]);
    }
    
    // get collection and store in collections cache
    const self = this;
    this.db.collection(collection, {strict: true}, (err, collectionCursor) => {
      if(err) return callback(err);
      self.collections[collection] = collectionCursor;
      callback(null, collectionCursor);
    });
  }
  
  findMaxValueOfField(collection, field, callback) {
    this.getCollection(
      collection,
      (err, collection) => {
        if(err) return callback(err);
        let
          query = {};
        query[field] = -1;
        collection
          .find()
          .sort(query)
          .limit(1)
          .next()
          .then(
            doc => callback(null, (doc) ? parseInt(doc[field]) : 0),
            err => callback(err));
      });
  }
  
  nextValueOf(collection, field, callback) {
    this.findMaxValueOfField(
      collection,
      field,
      (err, result) => {
        if(err) return callback(err);
        callback(null, ++result);
      });
  }
  
  findOne(collection, conditions, callback) {
    this.getCollection(
      collection,
      (err, collection) => {
        if(err) return callback(err);
        collection
          .findOne(conditions)
          .then(
            doc => callback(null, doc),
            err => callback(err));
      });
  }
  
  findMany(collection, conditions = {}, callback, page = 1, limit = 10) {
    this.getCollection(
      collection,
      (err, collection) => {
        if(err) return callback(err);
        collection
          .find(conditions)
          .skip((page - 1) * limit)
          .limit(limit)
          .toArray(callback);
      });
  }
  
  pickRandomOne(collection, callback, randomizedField) {
    this.pickRandomMany(collection, callback, randomizedField, 1);
  }
  
  pickRandomMany(collection, callback, randomizedField, limit = 10) {
    this.getCollection(
      collection,
      (err, collection) => {
        if(err) return callback(err);
        collection
          .count()
          .then(
            count => {
              let
                query = {};
                query[randomizedField] = {$gte: (_.random(0, count) - limit)};
              collection
                .find(query)
                .limit(limit)
                .toArray(callback);
            },
            err => {
              console.error('MongoClient.pickRandomMany err:', err);
              callback(err);
            });
      });
  }
  
  insertOne(collection, doc, callback) {
    this.getCollection(
      collection,
      (err, collection) => {
        if(err) return callback(err);
        collection.insertOne(doc, callback);
      });
  }
  
  insertMany(collection, docs, callback) {
    this.getCollection(
      collection,
      (err, collection) => {
        if(err) return callback(err);
        collection.insertMany(docs, callback);
      });
  }
  
  updateOne(collection, conditions, set, callback) {
    this.getCollection(
      collection,
      (err, collection) => {
        if(err) return callback(err);
        collection.updateOne(conditions, {$set: set}, {upsert: false}, callback);
      });
  }
  
  updateMany(collection, conditions, set, callback) {
    this.getCollection(
      collection,
      (err, collection) => {
        if(err) return callback(err);
        collection.updateMany(conditions, {$set: set}, callback);
      });
  }
  
  deleteOne(collection, conditions, operations, callback) {
    this.getCollection(
      collection,
      (err, collection) => {
        if(err) return callback(err);
        collection.deleteOne(conditions, operations, callback);
      });
  }
  
  deleteMany(collection, conditions, callback) {
    this.getCollection(
      collection,
      (err, collection) => {
        if(err) return callback(err);
        collection.deleteMany(conditions, callback);
      });
  }
  
  upsertOne(collection, document, queryFields = ['_id'], callback) {
    this.getCollection(
      collection,
      (err, collection) => {
        if(err) return callback(err);
        collection.updateOne(_.pick(document, queryFields), {$set: document}, {upsert: true}, callback);
      });
  }
  
  upsertMany(collection, documents, queryFields = ['_id'], callback) {
    const self = this;
    async.eachSeries(
      documents,
      (document, next) => self.upsertOne(collection, document, queryFields, next),
      callback);
  }
}

module.exports = Database;