/* jshint strict: true */

'use strict';

// common packages
const
  _ = require('lodash'),
  uuid = require('uuid'),
  path = require('path'),
  async = require('async');

// configuration
const
  DB_CONNECTION_URI = 'mongodb://127.0.0.1:27017/looper',
  SITE_URL = 'http://coub.com',
  TIMELINE_URL = SITE_URL+'/api/v2/timeline/explore',
  TIMELINE_FRESH_URL = TIMELINE_URL+'/newest',
  TIMELINE_HOTTEST_URL = TIMELINE_URL+'/hot',
  TIMELINE_RISING_URL = TIMELINE_URL+'/rising',
  TIMELINE_RANDOM_URL = TIMELINE_URL+'/random',
  CHANNELS_URL = 'http://coub.com/api/v2/channels/featured_channels',
  CHANNEL_URL = 'http://coub.com/api/v2/timeline/channel/',
  rootPath = path.join(__dirname, '..');

// requiring custom components
const
  FSComponent = require('./components/fs'),
  RequestComponent = require('./components/request'),
  DatabaseComponent = require('./components/database'),
  Hasher = require('./components/hasher');

const
  FS = new FSComponent(rootPath),
  Request = new RequestComponent(SITE_URL, SITE_URL),
  DB = new DatabaseComponent(DB_CONNECTION_URI);



/**** SOME COMMON METHODS ****/
function saveCoubs(coubs, callback) {
  if(_.isArray(coubs) && !_.isEmpty(coubs)) {
    return DB.upsertMany('coubs', coubs, ['id', 'permalink'], callback);
  }
  callback();
}
/**** END ****/



/**** COLLECTING RANDOM COUBS TO COUBS COLLECTION ****/
function getInitialUrlRandom() {
  return TIMELINE_RANDOM_URL;
}

function getPageUrlRandom(n) {
  return TIMELINE_RANDOM_URL+'?page='+n;
}

function getPagesCountRandom(callback) {
  Request.get(
    getInitialUrlRandom(),
    (err, response, body) => {
      callback(err, _.isNumber(body.total_pages) ? parseInt(body.total_pages) : 0);
    });
}

function getPageRandom(n, callback) {
  Request.get(
    getPageUrlRandom(n),
    (err, response, body) => {
      if(err) return callback(err);
      callback(null, body);
    });
}

function processPageRandom(n, callback) {
  console.log('RANDOM PAGE:', n);
  getPageRandom(n, (err, body) => {
    if(err) return callback(err);
    saveCoubs(_.isArray(body.coubs)?body.coubs:[], callback);
  })
}

function collectRandom(finished) {
  getPagesCountRandom((err, pages) => {
    if(err) {
      console.error('collectRandom => getPagesCountRandom err:', err);
      return finished();
    }
    
    if(pages > 5) pages = 5; // çox da şey eləmə
    
    console.log("\n\n", "RANDOM PAGES:", pages);

    async.eachSeries(
      _.range(1, pages+1),
      (page, next) =>
        setTimeout(() =>
          processPageRandom(page, next),
          _.random(5000, 10000)),
      (err) => {
        if(err) console.error('collectRandom => getPagesCountRandom => eachSeries err:', err);
        finished();
      });
  });
}
/**** END ****/



/**** COLLECTING NEWEST COUBS TO COUBS COLLECTION ****/
function getInitialUrlFresh() {
  return TIMELINE_FRESH_URL;
}

function getPageUrlFresh(n) {
  return TIMELINE_FRESH_URL+'?page='+n;
}

function getPagesCountFresh(callback) {
  Request.get(
    getInitialUrlFresh(),
    (err, response, body) => {
      callback(err, _.isNumber(body.total_pages) ? parseInt(body.total_pages) : 0);
    });
}

function getPageFresh(n, callback) {
  Request.get(
    getPageUrlFresh(n),
    (err, response, body) => {
      if(err) return callback(err);
      callback(null, body);
    });
}

function processPageFresh(n, callback) {
  console.log('FRESH PAGE:', n);
  getPageFresh(n, (err, body) => {
    if(err) return callback(err);
    saveCoubs(_.isArray(body.coubs)?body.coubs:[], callback);
  })
}

function collectFresh(finished) {
  getPagesCountFresh((err, pages) => {
    if(err) {
      console.error('collectFresh => getPagesCountFresh err:', err);
      return finished();
    }
    
    if(pages > 5) pages = 5; // qaqa beş dənə bəsdü
    
    console.log("\n\n", "FRESH PAGES:", pages);
    
    async.eachSeries(
      _.range(1, pages+1),
      (page, next) =>
        setTimeout(() =>
          processPageFresh(page, next),
          _.random(5000, 10000)),
      (err) => {
        if(err) console.error('collectFresh => getPagesCountFresh => eachSeries err:', err);
        finished();
      });
  });
}
/**** END ****/



/**** COLLECTING HOTTEST COUBS TO COUBS COLLECTION ****/
function getInitialUrlHottest() {
  return TIMELINE_HOTTEST_URL;
}

function getPageUrlHottest(n) {
  return TIMELINE_HOTTEST_URL+'?page='+n;
}

function getPagesCountHottest(callback) {
  Request.get(
    getInitialUrlHottest(),
    (err, response, body) => {
      callback(err, _.isNumber(body.total_pages) ? parseInt(body.total_pages) : 0);
    });
}

function getPageHottest(n, callback) {
  Request.get(
    getPageUrlHottest(n),
    (err, response, body) => {
      if(err) return callback(err);
      callback(null, body);
    });
}

function processPageHottest(n, callback) {
  console.log('HOTTEST PAGE:', n);
  getPageHottest(n, (err, body) => {
    if(err) return callback(err);
    saveCoubs(_.isArray(body.coubs)?body.coubs:[], callback);
  })
}

function collectHottest(finished) {
  getPagesCountHottest((err, pages) => {
    if(err) {
      console.error('collectHottest => getPagesCountHottest err:', err);
      return finished();
    }
    
    if(pages > 5) pages = 5; // qaqa beş dənə bəsdü
    
    console.log("\n\n", "HOTTEST PAGES:", pages);
    
    async.eachSeries(
      _.range(1, pages+1),
      (page, next) =>
        setTimeout(() =>
            processPageHottest(page, next),
          _.random(5000, 10000)),
      (err) => {
        if(err) console.error('collectHottest => getPagesCountHottest => eachSeries err:', err);
        finished();
      });
  });
}
/**** END ****/



/**** COLLECTING RISING COUBS TO COUBS COLLECTION ****/
function getInitialUrlRising() {
  return TIMELINE_RISING_URL;
}

function getPageUrlRising(n) {
  return TIMELINE_RISING_URL+'?page='+n;
}

function getPagesCountRising(callback) {
  Request.get(
    getInitialUrlRising(),
    (err, response, body) => {
      callback(err, _.isNumber(body.total_pages) ? parseInt(body.total_pages) : 0);
    });
}

function getPageRising(n, callback) {
  Request.get(
    getPageUrlRising(n),
    (err, response, body) => {
      if(err) return callback(err);
      callback(null, body);
    });
}

function processPageRising(n, callback) {
  console.log('RISING PAGE:', n);
  getPageRising(n, (err, body) => {
    if(err) return callback(err);
    saveCoubs(_.isArray(body.coubs)?body.coubs:[], callback);
  })
}

function collectRising(finished) {
  getPagesCountRising((err, pages) => {
    if(err) {
      console.error('collectRising => getPagesCountRising err:', err);
      return finished();
    }
    
    if(pages > 5) pages = 5; // qaqa beş dənə bəsdü
    
    console.log("\n\n", "RISING PAGES:", pages);
    
    async.eachSeries(
      _.range(1, pages+1),
      (page, next) =>
        setTimeout(() =>
            processPageRising(page, next),
          _.random(5000, 10000)),
      (err) => {
        if(err) console.error('collectRising => getPagesCountRising => eachSeries err:', err);
        finished();
      });
  });
}
/**** END ****/



/**** COLLECTING CHANNEL COUBS TO COUBS COLLECTION ****/
var channel;

function pickRandomChannel(callback) {
  Request.get(CHANNELS_URL,
    (err, response, body) => {
      if(err) return callback(err);
      
      let page = (body.total_pages > 1)? body.total_pages-1 : 1;
      page = _.random(1, page);
      Request.get(CHANNELS_URL+'?page='+page,
        (err, response, body) => {
          
          if(err) return callback(err);
          
          if(!_.isEmpty(body.channels)) {
            channel = body.channels[_.random(0, body.channels.length-1)];
            if(!_.isEmpty(channel)) {
              channel = channel.permalink;
              return callback();
            }
          }
          
          callback('No channels');
        });
    });
}

function getInitialUrlForChannel() {
  return CHANNEL_URL+channel;
}

function getPageUrlForChannel(n) {
  return CHANNEL_URL+channel+'?page='+n;
}

function getPagesCountForChannel(callback) {
  Request.get(
    getInitialUrlForChannel(),
    (err, response, body) => {
      callback(err, _.isNumber(body.total_pages) ? parseInt(body.total_pages) : 0);
    });
}

function getPageForChannel(n, callback) {
  Request.get(
    getPageUrlForChannel(n),
    (err, response, body) => {
      if(err) return callback(err);
      callback(null, body);
    });
}

function processPageForChannel(n, callback) {
  console.log('CHANNEL PAGE:', n);
  getPageForChannel(n, (err, body) => {
    if(err) return callback(err);
    saveCoubs(_.isArray(body.coubs)?body.coubs:[], callback);
  })
}

function collectForChannel(finished) {
  pickRandomChannel(err => {
    if(err) {
      console.error('collectForChannel => pickRandomChannel err:', err);
      return finished();
    }
  
    getPagesCountForChannel((err, pages) => {
      if(err) {
        console.error('collectForChannel => getPagesCountForChannel err:', err);
        return finished();
      }
    
      if(pages > 10) pages = 10; // qaqa on dənə bəsdü
    
      console.log("\n\n", "CHANNEL PAGES:", pages);
    
      async.eachSeries(
        _.range(1, pages + 1),
        (page, next) =>
          setTimeout(() =>
              processPageForChannel(page, next),
            _.random(5000, 10000)),
        (err) => {
          if(err) console.error('collectForChannel => getPagesCountForChannel => eachSeries err:', err);
          finished();
        });
    });
  });
}
/**** END ****/



/**** DOWNLOADING COUBS, GENERATING LOOPS ****/
function getRandomCoubs(callback) {
  DB.pickRandomMany('coubs', callback, 'id');
}

var getCoubsSequentialyPage = 0;
function getCoubsSequentialy(callback) {
  DB.findMany('coubs', {}, (err, coubs) => {
    if(err) return callback(err);
    if(coubs.length === 0) {
      getCoubsSequentialyPage--;
      return callback();
    }
    callback(null, coubs);
  }, ++getCoubsSequentialyPage, 20);
}

function pickVideos(coub) {
  let webVersion = coub.file_versions.web || {};
  let videos = [];
  if(!_.isEmpty(webVersion)) {
    let template = webVersion.template;
    let types = webVersion.types;
    let versions = webVersion.versions;
    for(let type of types) {
      for(let version of versions) {
        let
          video = _.replace(template, new RegExp('%{type}','g'), type);
          video = _.replace(video, new RegExp('%{version}','g'), version);
        videos.push({url: video, version, ext: type});
      }
    }
  }
  
  return videos;
}

function pickAudios(coub) {
  let audioVersions = coub.audio_versions || {};
  let audios = [];
  if(!_.isEmpty(audioVersions)) {
    let template = audioVersions.template;
    let versions = audioVersions.versions;
    for(let version of versions) {
      let audio = _.replace(template, new RegExp('%{version}','g'), version);
      audios.push({url: audio, version, ext: 'mp3'});
    }
  }
  
  return audios;
}

function pickImages(coub) {
  let imageVersions = coub.image_versions || {};
  let images = [];
  if(!_.isEmpty(imageVersions)) {
    let template = imageVersions.template;
    for(let version of ['micro', 'tiny', 'small', 'medium', 'big']) {
      let image = _.replace(template, new RegExp('%{version}','g'), version);
      images.push({url: image, version, ext: 'jpg'});
    }
  }
  return images;
}

function saveInfos(coub, loop, callback) {
  async.parallel([
      done =>
        DB.upsertMany('tags',
                      coub.tags, ['id'], done),
      
      done =>
        DB.upsertMany('categories',
                    _.map(coub.categories, category => {
                      category.title = {original: category.title};
                      category.slug = {original: _.kebabCase(category.title)};
                      return _.pick(category, ['id', 'title', 'slug']);
                    }), ['id'], done)
    ],
    (err, results) => {
      callback(err, (err)? true : false);
    });
}

function saveVideos(coub, loop, callback) {
  let videos = pickVideos(coub);
  let downloaded = {};
  async.eachSeries(
    videos,
    (video, next) => {
      let request = Request.requestObject(video.url);
      let file = FS.getLoopVideoPath(loop.uuid.split('-').join('/'), video.version, video.ext);
      FS.pipeRequestToFile(request, file, (err) => {
        if(err) return next();
        downloaded[video.version] = path.join(loop.uuid.split('-').join('/'), 'video', video.version+'.'+video.ext);
        next();
      });
    },
    (err) => {
      callback(null, downloaded);
    });
}

function saveAudios(coub, loop, callback) {
  let audios = pickAudios(coub);
  let downloaded = {};
  async.eachSeries(
    audios,
    (audio, next) => {
      let request = Request.requestObject(audio.url);
      let file = FS.getLoopAudioPath(loop.uuid.split('-').join('/'), audio.version, audio.ext);
      FS.pipeRequestToFile(request, file, (err) => {
        if(err) return next();
        downloaded[audio.version] = path.join(loop.uuid.split('-').join('/'), 'audio', audio.version+'.'+audio.ext);
        next();
      });
    },
    (err) => {
      callback(null, downloaded);
    });
}

function saveImages(coub, loop, callback) {
  let images = pickImages(coub);
  let downloaded = {};
  async.eachSeries(
    images,
    (image, next) => {
      let request = Request.requestObject(image.url);
      let file = FS.getLoopImagePath(loop.uuid.split('-').join('/'), image.version, image.ext);
      FS.pipeRequestToFile(request, file, (err) => {
        if(err) return next();
        downloaded[image.version] = path.join(loop.uuid.split('-').join('/'), 'images', image.version+'.'+image.ext);
        next();
      });
    },
    (err) => {
      callback(null, downloaded);
    });
}

function loopExists(query, callback) {
  DB.findOne(
    'loops',
    {"coub.id": query.id, "coub.permalink": query.permalink},
    (err, loop) => {
      if(err) return callback(err);
      callback(null, loop, (loop)? true : false);
    });
}

function prepareNewLoopFromCoub(coub, callback) {
  DB.nextValueOf(
    'loops', 'id',
    (err, id) => {
      if(err) return callback(err);
  
      let
        data = {};
        data.id = id;
        data.uuid = uuid.v4();
        data.title = {original: coub.title};
        data.slug = {original: _.kebabCase(coub.title)};
        data.video = {};
        data.audio = {};
        data.images = {};
        data.real_video =
          (coub.media_blocks && coub.media_blocks.external_video)?
            coub.media_blocks.external_video : null;
        data.tags = _.map(coub.tags, tag => tag.value);
        data.categories = _.map(coub.categories, category => category.id);
        data.views_count = coub.views_count;
        data.like_count = 0;
        data.share_count = 0;
        data.comments = [];
        data.failed = false;
        data.ready = false;
        data.active = false;
        data.deleted = false;
        data.createdAt = new Date();
        data.updatedAt = new Date();
        data.coub = _.pick(coub, ['id', 'permalink', 'title']);
        callback(null, data);
    });
}

function createLoopFromCoub(coub, callback) {
  loopExists(
    coub,
    (err, loop, exists) => {
      if(err) return callback(err);
      if(exists === true) {
        return callback(null, loop);
      }
      
      prepareNewLoopFromCoub(coub, (err, loop) => {
        if(err) return callback(err);
        DB.insertOne('loops', loop, (err, result) => {
          if(err) return callback(err);
          DB.findOne('loops', {id: loop.id}, callback);
        });
      });
    });
}

function setLoopDeleted(loop, callback) {
  if(!callback) callback = () => {};
  DB.updateOne('loops', {_id: loop._id}, {updatedAt: new Date(), deleted: true}, callback);
}

function setLoopFailed(loop, callback) {
  if(!callback) callback = () => {};
  DB.updateOne('loops', {_id: loop._id}, {updatedAt: new Date(), failed: true}, callback);
}

function setLoopReady(loop, callback) {
  if(!callback) callback = () => {};
  DB.updateOne('loops', {_id: loop._id}, {updatedAt: new Date(), ready: true}, callback);
}

function setLoopActive(loop, callback) {
  if(!callback) callback = () => {};
  DB.updateOne('loops', {_id: loop._id}, {updatedAt: new Date(), active: true}, callback);
}

function processCoub(coub, callback) {
  createLoopFromCoub(coub, (err, loop) => {
    if(err) return callback(err);
    
    //console.log('Processing coub:', coub.id, coub.permalink, coub.title);
    if(loop.deleted === true) {
      //console.log('Loop with related coub:', coub.id, coub.permalink, coub.title, 'was set to delete. skipping...');
      return callback();
    }
    
    if(loop.failed === true) {
      //console.log('Loop with related coub:', coub.id, coub.permalink, coub.title, 'was set to failed state. skipping...');
      return callback();
    }
    
    if(loop.ready === true) {
      //console.log('Loop with related coub:', coub.id, coub.permalink, coub.title, 'is ready to consume. skipping...');
      return callback(null, true);
    }
    
    FS.createLoopPath(loop.uuid.split('-').join('/'), (err) => {
      if(err) return callback(err);
  
      async.parallel([
          done => saveInfos(coub, loop, done),
          done => saveVideos(coub, loop, done),
          done => saveAudios(coub, loop, done),
          done => saveImages(coub, loop, done)
        ],
        (err, results) => {
          if(err) {
            return callback(err);
          }
      
          let video = results[1];
          let audio = results[2];
          let images = results[3];
          
          let set = {};
          set.video = video;
          set.audio = audio;
          set.images = images;
          set.updatedAd = new Date();
          set.failed = false;
          set.ready = true;
          set.active = true;
          DB.updateOne(
            'loops',
            {_id: loop._id},
            set,
            (err) => {
              if(err) return callback(err);
  
              console.log('Coub:', coub.id, coub.permalink, coub.title, 'saved as loop successfully');
              callback();
            });
        });
      });
    });
}

function download(finished) {
  getCoubsSequentialy((err, coubs) => {
    if(err) {
      console.error('download => getCoubsSequentialy err:', err);
      return finished();
    }

    if(!coubs) return finished();
    
      //console.log("\n", "GOT COUBS:", coubs.length);
    let skips = 0;
    async.eachSeries(
      coubs,
      (coub, next) => {
        processCoub(coub, (err, skip) => {
          if(err) return next(err);
          if(skip) {
            skips++;
            return next();
          }
          setTimeout(next, _.random(1000, 3000));
        });
      },
      (err) => {
        if(err) console.error('download => getRandomCoubs => eachSeries err:', err);
        finished((skips == coubs.length)? true : false);
      });
  });
}
/**** END ****/



/**** STARTING THE GAME (: ****/
function checkDBAndStart() {
  if(DB.connected()) {
    
    async.forever(
      (repeat) =>
        collectForChannel(() => setTimeout(repeat, 5*60*1000)));
    
    async.forever(
      (repeat) =>
        collectRandom(() => setTimeout(repeat, 24*60*60*1000)));
     
    async.forever(
      (repeat) =>
        collectFresh(() => setTimeout(repeat, 2*60*60*1000)));
  
    async.forever(
      (repeat) =>
        collectHottest(() => setTimeout(repeat, 12*60*1000)));
  
    async.forever(
      (repeat) =>
        collectRising(() => setTimeout(repeat, 6*60*1000)));
    
    async.forever(
      (repeat) =>
        download((now) => (!now)? setTimeout(repeat, 1000) : repeat()));
    
    clearInterval(interval);
  }
}

var interval = setInterval(checkDBAndStart, 1000);



